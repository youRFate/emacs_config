# Modular EMACS configuration
This repository contains my configuration for
[GNU Emacs](https://www.gnu.org/software/emacs/emacs.html). Because it's grown
quite lengthy I've split it into several different files.

## Installation
Clone this repository into your home directory and name it `.emacs.d`:

`git clone https://gitlab.com/youRFate/emacs_config.git .emacs.d`

If you use a Proxy for internet connection, rename
`my_init/my_proxies.el.example` by removing the `.example` extension and adjust
the settings in the file. The file will then be loaded automatically.

On the first startup emacs will download a number of packages, which are defined
in `my_init/my_load_simple_modes.el`. It may be necessary to restart emacs once
this operation is complete to get all packages to load correctly.

On Windows systems it may be necessary to change the owner of the
`.emacs.d/server` directory from *Administrators* to your user.

## License

        Copyright (C) 2009-2019 Christoph Paa

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
