;;  .----------------.  .----------------.  .----------------.  .----------------.  .----------------. 
;; | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
;; | |  _________   | || | ____    ____ | || |      __      | || |     ______   | || |    _______   | |
;; | | |_   ___  |  | || ||_   \  /   _|| || |     /  \     | || |   .' ___  |  | || |   /  ___  |  | |
;; | |   | |_  \_|  | || |  |   \/   |  | || |    / /\ \    | || |  / .'   \_|  | || |  |  (__ \_|  | |
;; | |   |  _|  _   | || |  | |\  /| |  | || |   / ____ \   | || |  | |         | || |   '.___`-.   | |
;; | |  _| |___/ |  | || | _| |_\/_| |_ | || | _/ /    \ \_ | || |  \ `.___.´\  | || |  |`\____) |  | |
;; | | |_________|  | || ||_____||_____|| || ||____|  |____|| || |   `._____.´  | || |  |_______.'  | |
;; | |              | || |              | || |              | || |              | || |              | |
;; | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
;;  '----------------'  '----------------'  '----------------'  '----------------'  '----------------' 
;; modular init.el file

;; Copyright (C) 2009-2018 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
                                        ;(package-initialize)

;; make custom init files available for loading
(add-to-list 'load-path "~/.emacs.d/my_init")

;; load proxy settings if a custom file was created
(when (file-exists-p "~/.emacs.d/my_init/my_proxies.el")
  (require 'my_proxies))

;; load modes that do not have much / any config
(require 'my_load_simple_modes)

;; load basic emacs configurations
(require 'my_basic_emacs_conf)

;; load theme settings
;;(when (display-graphic-p)
(require 'my_themes)

;; load custom file->mode associations
(require 'my_file_associations)

;; load my AUCTeX configuration & mode
(require 'my_auctex)

;; load flyspell configuration
(require 'my_flyspell)


;; Configuration that is local to this machine, load if exists
(when (file-exists-p "~/.emacs.d/my_init/my_workstation_conf.el")
  (require 'my_workstation_conf))
