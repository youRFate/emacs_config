(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(custom-safe-themes
   '("06ed754b259cb54c30c658502f843937ff19f8b53597ac28577ec33bb084fa52"
     "0170347031e5dfa93813765bc4ef9269a5e357c0be01febfa3ae5e5fcb351f09"
     "3c7a784b90f7abebb213869a21e84da462c26a1fda7e5bd0ffebf6ba12dbd041"
     "249e100de137f516d56bcf2e98c1e3f9e1e8a6dce50726c974fa6838fbfcec6b"
     "65809263a533c5151d522570b419f1a653bfd8fb97e85166cf4278e38c39e00e"
     "7dc1c6210efe106a8c6cd47009a2ffd0069826b550dda379e8e4ef6105384cba"
     "046a2b81d13afddae309930ef85d458c4f5d278a69448e5a5261a5c78598e012"
     "5e05db868f138062a3aedcccefe623eee18ec703ae25d4e5aebd65f892ac5bcc"
     "04aa1c3ccaee1cc2b93b246c6fbcd597f7e6832a97aaeac7e5891e6863236f9f"
     "5f92b9fc442528b6f106eaefa18bb5e7bfa0d737164e18f1214410fef2a6678d"
     "628278136f88aa1a151bb2d6c8a86bf2b7631fbea5f0f76cba2a0079cd910f7d"
     "939ea070fb0141cd035608b2baabc4bd50d8ecc86af8528df9d41f4d83664c6a"
     "aded61687237d1dff6325edb492bde536f40b048eab7246c61d5c6643c696b7f"
     "dcdd1471fde79899ae47152d090e3551b889edf4b46f00df36d653adc2bf550d"
     "4cf9ed30ea575fb0ca3cff6ef34b1b87192965245776afa9e9e20c17d115f3fb"
     "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e"
     "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa"
     default))
 '(display-time-mode t)
 '(package-selected-packages
   '(ag aggressive-indent auctex auto-compile autopair
        color-theme-sanityinc-tomorrow company-irony counsel
        dockerfile-mode dracula-theme ef-themes elpy expand-region flx
        flycheck google-c-style gruvbox-theme guru-mode
        highlight-indent-guides iedit jedi kaolin-themes lua-mode
        magit-todos markdown-mode moe-theme monokai-theme pet
        powerline powershell py-autopep8 rainbow-delimiters rg
        rust-mode smart-mode-line vhdl-ext vterm w32-browser yaml-mode
        zenburn-theme))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "PragmataPro Mono Liga" :foundry "FSD " :slant normal :weight regular :height 113 :width normal)))))
