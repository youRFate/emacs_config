;;Testcase mode for FPGA testbench testcase syntax highlighting

;; Copyright (C) 2009-2017 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;activate using 'M-x tc-mode'
;;or add the folowing to your emacs config:
;;(add-to-list 'auto-mode-alist '("tc\[0-9\]\[0-9\]\[0-9\]\[0-9\]\.*\\.txt\\'" . tc-mode))
;; this will auto load tc-mode in all files named 'tc****<?>.txt', such as "tc3000.txt" or "report_tc2000_2014-03-06_12-31.txt"

;;lists of keyword classes and keywords
(setq tc-time '("us" "ps" "ms" "ns" "tcbframe" "rmframe" "clk"))
(setq tc-events '("RMINTEVENT_TC" "RMINTEVENT_TU" "RMINTEVENT" "TCBEVENT_IN" "TCB" "REGISTERVALUE_IN" "REGISTERVALUE" "SPIDATA_IN" "SPIDATA" "WRITE" "READ" "RMINTTURESP"))

;;create regular expressions for keyword lists
(setq tc-time-regexp (regexp-opt tc-time 'words))
(setq tc-events-regexp (regexp-opt tc-events 'words))

;;clear some memory
(setq tc-time nil)
(setq tc-events nil)

;; create the list for font-lock.
;; each class of keyword is given a particular face
(setq tc-font-lock-keywords
  `(
    ("<TC>\\|</TC>\\|<STIMULUS>\\|</STIMULUS>\\|<EXPECTATION>\\|</EXPECTATION>\\|<ID>\\|</ID>" . font-lock-function-name-face)
    (,tc-time-regexp . font-lock-type-face)
    (,tc-events-regexp . font-lock-keyword-face)
))

;; command to comment/uncomment text, binding see TC mode menu.
(defun tc-comment-dwim (arg)
  "Comment or uncomment current line or region in a smart way.
For detail, see `comment-dwim'."
  (interactive "*P")
  (require 'newcomment)
  (let (
        (comment-start ";") (comment-end "")
        )
    (comment-dwim arg)))

;; syntax table for comment highlighting
(defvar tc-syntax-table nil "Syntax table for `tc-mode'.")
(setq tc-syntax-table
      (let ((synTable (make-syntax-table)))
        (modify-syntax-entry ?\; "<" synTable)
        (modify-syntax-entry ?\n ">" synTable)

        synTable))

(defvar tc-mode-map nil "Keymap for tc-mode")
;; definition for your keybinding and menu
(when (not tc-mode-map) ; if it is not already defined
  ;; assign command to keys
  (setq tc-mode-map (make-sparse-keymap))
  (define-key tc-mode-map [remap comment-dwim] 'tc-comment-dwim)
   ; above: make your comment command “xlsl-comment-dwim” use the current key for “comment-dwim” (because user may have changed the key for “comment-dwim”)

  ;; define your menu
  (define-key tc-mode-map [menu-bar] (make-sparse-keymap))

  (let ((menuMap (make-sparse-keymap "TC")))
    (define-key tc-mode-map [menu-bar tc] (cons "TC" menuMap))
    (define-key menuMap [comment-region]
      '("Comment region" . tc-comment-dwim))))

(define-derived-mode tc-mode fundamental-mode
  "tc-mode is a major mode for editing testcase files"
  :syntax-table tc-syntax-table
  (setq font-lock-defaults '(tc-font-lock-keywords))
  (setq mode-name "testcase-mode")
  (use-local-map tc-mode-map)
)
(provide 'tc-mode)

