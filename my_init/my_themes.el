;; Copyright (C) 2009-2018 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;Themes to load
;;(load-theme 'solarized-dark t)
;;(load-theme 'solarized-light t)
;;(load-theme 'zenburn t)
;;(load-theme 'material)

(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

;;powerline mode
;;(require 'powerline)
;;(setq powerline-arrow-shape 'curve)

;;customize theme
;;(require 'moe-theme)
;;(moe-dark)
;;(powerline-moe-theme)
;;(moe-theme-set-color 'green)
;;(setq powerline-separator-style 'wave)

;; load theme
(load-theme 'kaolin-temple t)
;; find stuff with describe-char, then fix it.
(set-face-attribute 'fixed-pitch nil :family 'unspecified)
(set-face-attribute 'diredp-omit-file-name nil :strike-through 'unspecified)
;;(load-theme 'dracula t)

;; load smart mode line
(sml/setup)

;; mode line setup
(setq display-time-format "%H:%M")
(display-time-mode)
;;(powerline-default-theme)

;;Paren mode setup
;;(show-paren-mode t)
;;(setq show-paren-style 'expression)

(provide 'my_themes)
