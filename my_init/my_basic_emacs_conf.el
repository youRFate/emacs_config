;; Copyright (C) 2009-2018 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;some basic emacs settings

(require 'server)
(or (server-running-p)
		(server-start))

;; make this gig a fast one
;(byte-recompile-directory (expand-file-name "~/.emacs.d") 0)

;;customize-mode
(setq custom-file "~/.emacs.d/emacs-custom.el")
(load custom-file)

;;save state
(setq desktop-path '("~/.emacs.d"))
(setq desktop-dirname "~/.emacs.d")
(setq desktop-base-file-name "emacs-desktop")
(desktop-save-mode 0)

;;configure tramp mode
(setq tramp-default-method "ssh")
(setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")
(setq tramp-verbose 6)
;;(add-to-list 'tramp-remote-path "/usr/bin")
;;(setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")

;;visual bell mode
(setq visible-bell 1)

;;no toolbar mode
(tool-bar-mode -1)

;;highlight line
(global-hl-line-mode)

;;line numbers
                                        ;(setq linum-format "%4d")
                                        ;(global-nlinum-mode 1)
                                        ;(require 'hlinum)
                                        ;(hlinum-activate)

;;unicode = standart
(prefer-coding-system 'utf-8)

;;whitespace mode, highlight too long lines
(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(setq whitespace-line-column 88)
;;turn it off for certain modes
(setq whitespace-global-modes '(not org-mode LaTeX-mode vterm-mode))
(global-whitespace-mode t)

;;windmove and framemove keybindings and hooks
;(require 'framemove)
(windmove-default-keybindings 'meta)
(setq framemove-hook-into-windmove t)

;;allow upcase and downcase region commands
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;;column number
(column-number-mode 1)

;;highlicht parenthesis
(show-paren-mode t)

;;switch frames with M-`\
(global-set-key "\M-`" 'other-frame)

;;custom tab width
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

;;move the annoying *~ files into one directory.
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5    ; and how many of the old
  )

(provide 'my_basic_emacs_conf)
