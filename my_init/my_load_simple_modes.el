;; Copyright (C) 2009-2019 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;load modes that do not reqire much config

;;load plugins in plugin root
(add-to-list 'load-path "~/.emacs.d/plugins")

;;package manager
(setq user-home-directory "/home/paa_chr/")
(setq work-p nil)
(require 'package)
(if work-p
    (add-to-list 'package-archives '("melpa" . "https://code.rsint.net/mirror/github.com/d12frosted/elpa-mirror/raw/master/melpa/" ) t)
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))
(package-initialize)

; list of packages that will be installed if not already present
(let ((pkgs-to-install
       (seq-remove 'package-installed-p
		   '(auto-compile guru-mode magit magit-todos w32-browser
                     ag moe-theme zenburn-theme
                     monokai-theme powerline iedit auctex
                     rainbow-delimiters aggressive-indent gruvbox-theme
                     aggressive-indent smart-mode-line lua-mode
                     highlight-indent-guides powershell markdown-mode
                     rust-mode dracula-theme vterm rg
                     pet company yasnippet kaolin-themes ef-themes vhdl-ext))))
  (when pkgs-to-install
    (package-refresh-contents)
    (dolist (pkg pkgs-to-install)
      (package-install pkg))))

(setq load-prefer-newer t)
(require 'auto-compile)
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)

;; global aggressive indent
;; (global-aggressive-indent-mode 1)
;;(add-to-list 'aggressive-indent-excluded-modes 'html-mode)

;; rg mode
(require 'rg)
;; (rg-enable-default-bindings)
(rg-enable-menu)

;; expand region selection
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;;powerline mode
(require 'powerline)

;;mode for testcase files
(require 'tc-mode)

;;iedit for editing mulitple occurrances of one string
(require 'iedit)

;;org-mode, maybe move to separate file if it gets too big...
(require 'org)
(setq org-todo-keywords
      '((sequence "TODO" "VERIFY" "|" "DONE" "DELEGATED")))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
;; sum all checkboxes, not just top-most hierarchy
(setq org-hierarchical-checkbox-statistics nil)
;; fontify code in code blocks
(setq org-src-fontify-natively t)

;;git interface
(global-set-key "\C-xg" 'magit-status)
(setenv "GIT_ASKPASS" "git-gui--askpass")

;;improved dired, with some ls switches
(require 'dired+)
(setq diredp-hide-details-initially-flag nil)
(setq dired-listing-switches "-alhGn")


;; IBuffer mode
(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)

;;guru mode settings
(guru-global-mode +1)
;;(setq guru-warn-only t)

;; highligh indentation
(when (display-graphic-p)
  (setq highlight-indent-guides-method 'character)
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))


;; Windows performance tweaks
;;
(when (boundp 'w32-pipe-read-delay)
  (setq w32-pipe-read-delay 0))
;; Set the buffer size to 64K on Windows (from the original 4K)
(when (boundp 'w32-pipe-buffer-size)
  (setq irony-server-w32-pipe-buffer-size (* 64 1024)))

;; vhdl ls
(setq lsp-vhdl-server 'vhdl-ls)
(setq lsp-vhdl-server-path "/home/paa_chr/.cargo/bin/vhdl_ls")


;; ligatures in pragmata pro
;; Load pragmatapro-lig.el
(add-to-list 'load-path "~/.emacs.d/plugins/emacs-pragmatapro-ligatures")
(require 'pragmatapro-lig)

;; Enable pragmatapro-lig-mode for specific modes
(add-hook 'text-mode-hook 'pragmatapro-lig-mode)
(add-hook 'prog-mode-hook 'pragmatapro-lig-mode)
;; or globally
;;(pragmatapro-lig-global-mode)

;; treesitter mode
(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     (go "https://github.com/tree-sitter/tree-sitter-go")
     (html "https://github.com/tree-sitter/tree-sitter-html")
     (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
;; uncomment to compile all lang grammars
;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))
;; remap to ts modes
(setq major-mode-remap-alist
 '((yaml-mode . yaml-ts-mode)
   (bash-mode . bash-ts-mode)
   (js2-mode . js-ts-mode)
   (typescript-mode . typescript-ts-mode)
   (json-mode . json-ts-mode)
   (css-mode . css-ts-mode)
   (python-mode . python-ts-mode)))

;; python
(require 'pet)
(add-hook 'python-base-mode-hook 'pet-mode -10)

;; eglot
(require 'eglot)
(add-to-list 'eglot-server-programs '(python-ts-mode . ("/home/paa_chr/.local/bin/pyright-langserver" "--stdio")))

(setq vhdl-ext-feature-list
      '(font-lock
        xref
        capf
        hierarchy
        eglot
        flycheck
        beautify
        navigation
        template
        compilation
        imenu
        which-func
        hideshow
        time-stamp
        ports))
(require 'vhdl-ext)

(provide 'my_load_simple_modes)
