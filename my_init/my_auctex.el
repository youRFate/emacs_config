;; Copyright (C) 2009-2018 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; AUCTeX
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'visual-line-mode)
;; (add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

(setq TeX-PDF-mode t)

;; Automatically activate folding mode in auctex, use C-c C-o C-b to fold.
(add-hook 'TeX-mode-hook
					(lambda () (TeX-fold-mode 1))); Automatically activate TeX-fold-mode.

;; Enalbe electric pair mode for latex, pair math $ smybols.
(electric-pair-mode)
(add-hook 'LaTeX-mode-hook
          '(lambda ()
						 (define-key LaTeX-mode-map (kbd "$") 'self-insert-command)))

;; Math mode options
(setq TeX-electric-sub-and-superscript t)

;; Load german inspell if babel is german
;(add-hook 'TeX-language-de-hook
;      (lambda () (ispell-change-dictionary "german")))

(provide 'my_auctex)
