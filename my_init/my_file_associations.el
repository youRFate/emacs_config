;; Copyright (C) 2009-2018 Christoph Paa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;custom file associations

;;load binary files in hexl mode
(setq auto-mode-alist (cons '("\\.bin$" . hexl-mode) auto-mode-alist))

;;load .do files in TCL mode
(setq auto-mode-alist (cons '("\\.do$" . tcl-mode) auto-mode-alist))

;;load testcase files in tc-mode
(add-to-list 'auto-mode-alist '("tc\[0-9\]\[0-9\]\[0-9\]\[0-9\]\.*\\.txt\\'" . tc-mode))


(provide 'my_file_associations)
